import os, platform, webbrowser
vpn = "192.168.1.106"
link = "google.com"

# MacOS
#chrome_path = 'open -a /Applications/Google\ Chrome.app %s'

# Windows
chrome_path = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'

# Linux
# chrome_path = '/usr/bin/google-chrome %s'

def ping(host):
    if  platform.system().lower()=="windows":
        ping_str = "-n 1"
    else:
        ping_str = "-c 1"

    resposta = os.system("ping " + ping_str + " " + host)
    return resposta == 0

flag = False
while 1:
    if not ping(vpn):
        flag = False
        print("Not conected")
    else:
        if flag:
            print("Browser already open")
        else:
            print("Opening browser")
            webbrowser.get(chrome_path).open(link, new=0, autoraise=True)
            flag = True